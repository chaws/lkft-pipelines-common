#!/bin/bash

OE_DISTRO=${OE_DISTRO:-"lkft"}
OE_IMAGES=${OE_IMAGES:-"rpb-console-image-lkft"}
OE_MACHINE=${OE_MACHINE:-"juno"}
OE_RELEASE=${OE_RELEASE:-"sumo"}
SRCREV=${SRCREV:-"87d888a197db9443026045ba3c253a1a929114d4"}
TUXOE_CONTAINER=${TUXOE_CONTAINER:-"ubuntu-16.04"}
TUXOE_URL="https://oebuilds.tuxbuild.com"

case "${REPO_NAME}" in
  linux-mainline)
    kernel_recipe="linux-generic-mainline"
    kernel_recipe_version="git%"
    ;;
  linux-next)
    kernel_recipe="linux-generic-next"
    kernel_recipe_version="git%"
    ;;
  linux-stable-rc | linux-stable)
    kernel_recipe="linux-generic-stable-rc"
    if [ -v KERNEL_BRANCH ]; then
      major_minor="$(echo "${KERNEL_BRANCH}" | sed -e 's#^linux-##' | cut -d\. -f1,2)"
      kernel_recipe_version="${major_minor}+git%"
    fi
    ;;
esac

cat << EOF > tux.json
{
  "container": "${TUXOE_CONTAINER}",
  "sources": {
    "repo": {
      "branch": "${OE_RELEASE}",
      "manifest": "default.xml",
      "url": "https://gitlab.com/Linaro/lkft/rootfs/lkft-manifest"
    }
  },
  "envsetup": "setup-environment",
  "distro": "${OE_DISTRO}",
  "machine": "${OE_MACHINE}",
  "target": "${OE_IMAGES}",
  "local_conf": [
    "SRCREV_kernel = \"${SRCREV}\"",
    "PREFERRED_PROVIDER_virtual/kernel = \"${kernel_recipe}\"",
    "PREFERRED_VERSION_linux-generic-mainline = \"${kernel_recipe_version}\""
  ],
  "artifacts": [
    "\$DEPLOY_DIR"
  ]
}
EOF

cat tux.json

tuxsuite bake submit tux.json | tee logs.txt
# shellcheck disable=SC2002
uid=$(cat logs.txt | grep uid: | cut -d " " -f2)
echo "TUXOE_URL=${TUXOE_URL}/${uid}" | tee build-oe.json
echo "OE_MACHINE=${OE_MACHINE}" | tee -a build-oe.json
