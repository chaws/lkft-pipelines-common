#!/bin/bash

set -x

BUILD_OE=${BUILD_OE:-"build-oe.json"}

# shellcheck disable=SC2002
base_url=$(cat "${BUILD_OE}" | grep TUXOE_URL | awk -F'=' '{print $NF}')
# shellcheck disable=SC2002
oemachine=$(cat "${BUILD_OE}" | grep OE_MACHINE | awk -F'=' '{print $NF}')
ext="tar.xz"

case "${DEVICE_TYPE}" in
    #bcm2711-rpi-4-b)  ext=""   ;;
    dragonboard-410c) ext="ext4.gz" ;;
    dragonboard-845c) ext="ext4.gz" ;;
    hi6220-hikey)     ext="ext4.gz" ;;
    #i386)             ext=""   ;;
    #juno-r2)          ext=""   ;;
    #nxp-ls2088)       ext=""   ;;
    x15)              ext="ext4.gz" ;;
    #x86)              ext=""   ;;
    qemu_arm)         ext="ext4.gz" ;;
    qemu_arm64)       ext="ext4.gz" ;;
    qemu_i386)        ext="ext4.gz" ;;
    qemu_x86_64)      ext="ext4.gz" ;;
esac

export ROOTFS_URL="${base_url}/images/${oemachine}/rpb-console-image-lkft-${oemachine}.${ext}"
