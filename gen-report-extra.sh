#!/bin/bash

get_project_suffix() {
  local suffix=""
  if grep -q sanity <<< "${QA_PROJECT}"; then
    suffix=" - sanity"
  fi

  echo "${suffix}"
}
