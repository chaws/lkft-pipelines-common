#!/bin/bash

set -euxo pipefail

stage="build"
if [ $# -gt 0 ]; then
  stage="$1"
fi

project_id="${CI_PROJECT_PATH//\//%2F}"
project_url="https://gitlab.com/api/v4/projects/${project_id}"

# get list of jobs for this project
curl -f --silent \
     --output jobs.json \
     "${project_url}/pipelines/${CI_PIPELINE_ID}/jobs?per_page=100"

job_id="$(jq ".[] | select(.stage==\"${stage}\" and .status!=\"manual\").id" jobs.json | head -n1)"

# get the job artifacts
curl -f --silent \
     --output artifacts.zip \
     --location "${project_url}/jobs/${job_id}/artifacts"

# extract the files we want
unzip artifacts.zip build.json
