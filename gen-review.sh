#!/bin/bash

set -euxo pipefail

source "gen-report-extra.sh"

git_describe="$(jq -r '.[0].git_describe' build.json)"

suffix="$(get_project_suffix)"

build-email \
        --msg-from="${REVIEW_FROM}" \
        --msg-to="${REVIEW_TO}" \
        --msg-cc="${REVIEW_CC}" \
        --msg-subject="[review] ${QA_TEAM} ${REPORT_TYPE} for ${git_describe}${suffix}" \
        --msg-body="${REPORT_TYPE}.txt" \
        --msg-body-pre="Go here to send the report upstream ${CI_PIPELINE_URL}." > review.txt
